package Week1Day2;

import org.openqa.selenium.chrome.ChromeDriver;

public class NewLogin {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		 ChromeDriver driver= new ChromeDriver();
		 driver.manage().window().maximize();
		 driver.get("http://leaftaps.com/opentaps");
		 driver.findElementById("username").sendKeys("DemoSalesManager");
		 driver.findElementById("password").sendKeys("crmsfa");
		 driver.findElementByClassName("decorativeSubmit").click();
		 driver.findElementByLinkText("CRM/SFA").click();
		 driver.findElementByLinkText("Leads").click();
		 driver.findElementByLinkText("Create Lead").click();
		 driver.findElementById("createLeadForm_companyName").sendKeys("tcs");
		 driver.findElementById("createLeadForm_firstName").sendKeys("aasha");
		 driver.findElementById("createLeadForm_lastName").sendKeys("reddy");
		 driver.findElementById("createLeadForm_dataSourceId").sendKeys("Employee");
		 driver.findElementById("createLeadForm_marketingCampaignId").sendKeys("Automobile");
		 driver.findElementById("createLeadForm_firstNameLocal").sendKeys("barath");
		 driver.findElementById("createLeadForm_lastNameLocal").sendKeys("babu");
		 driver.findElementById("createLeadForm_personalTitle").sendKeys("normal");
		 driver.findElementById("createLeadForm_generalProfTitle").sendKeys("title");
		 driver.findElementById("createLeadForm_departmentName").sendKeys("cse");
		 driver.findElementById("createLeadForm_annualRevenue").sendKeys("100000");
		 driver.findElementById("createLeadForm_currencyUomId").sendKeys("USD-American Dollar");
		 driver.findElementById("createLeadForm_industryEnumId").sendKeys("ComputerHardware");
		 driver.findElementById("createLeadForm_numberEmployees").sendKeys("5");
		 driver.findElementById("createLeadForm_ownershipEnumId").sendKeys("Corporation");
		 driver.findElementById("createLeadForm_sicCode").sendKeys("12345");
		 driver.findElementById("createLeadForm_tickerSymbol").sendKeys("rational");
		 driver.findElementById("createLeadForm_description").sendKeys("working at good speed");
		 driver.findElementById("createLeadForm_importantNote").sendKeys("check warning");
		 driver.findElementById("createLeadForm_primaryPhoneCountryCode").sendKeys("1");
		 driver.findElementById("createLeadForm_primaryPhoneNumber").sendKeys("4355267");
		 driver.findElementById("createLeadForm_primaryPhoneAreaCode").sendKeys("044");
		 driver.findElementById("createLeadForm_primaryPhoneExtension").sendKeys("extention");
		 driver.findElementById("createLeadForm_primaryPhoneAskForName").sendKeys("askfor");
		 driver.findElementById("createLeadForm_primaryEmail").sendKeys("asd@gmail.com");
		 driver.findElementById("createLeadForm_primaryWebUrl").sendKeys("http://testleaf.com/document");
		 driver.findElementById("createLeadForm_generalToName").sendKeys("abs");
		 driver.findElementById("createLeadForm_generalAttnName").sendKeys("axa");
		 driver.findElementById("createLeadForm_generalAddress1").sendKeys("ghandi nagar");
		 driver.findElementById("createLeadForm_generalAddress2").sendKeys("gopalapuram st");
		 driver.findElementById("createLeadForm_generalCity").sendKeys("madurai");
		 driver.findElementById("createLeadForm_generalStateProvinceGeoId").sendKeys("Guam");
		 driver.findElementById("createLeadForm_generalPostalCode").sendKeys("600001");
		 driver.findElementById("createLeadForm_generalCountryGeoId").sendKeys("India");
		 driver.findElementById("createLeadForm_generalPostalCodeExt").sendKeys("022");
		 driver.findElementByClassName("smallSubmit").click();

	}

}
